from django.urls import path
from . import views
from .views import index

urlpatterns = [
    path('', views.index, name='index'),
    path('Halaman1.html', views.index, name='index'),
    path('Halaman2.html', views.hobi, name='hobi'),
    path('Halaman3.html', views.kucing, name='kucing'),
    path('base_tes.html', views.kucing1, name='kucing1')
]